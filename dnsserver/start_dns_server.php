<?php

require "vendor/autoload.php";

// JSON formatted DNS records file
$mysqlStorageProvider = new yswery\DNS\MySQLStorageProvider("domainconnect");

// Recursive provider acting as a fallback to the JsonStorageProvider
$recursiveProvider = new yswery\DNS\RecursiveProvider;

$stackableResolver = new yswery\DNS\StackableResolver(array($mysqlStorageProvider, $recursiveProvider));

// Creating a new instance of our class
$dns = new yswery\DNS\Server($stackableResolver);

// Starting our DNS server
$dns->start();
