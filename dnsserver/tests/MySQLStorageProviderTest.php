<?php

use yswery\DNS\MySQLStorageProvider;

class MySQLStorageProviderTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var yswery\DNS\JsonStorageProvider
   */
  protected $storage;

  public function setUp()
  {
    $this->storage = new MySQLStorageProvider('domainconnect');
  }

  public function DNSQuestionsProvider() {
    return [
      'question domain A record'        => [
        [['qname' => 'unit-test-domain-cf18.com', 'qtype' => 1, 'qclass' => 1]],
        [['name' => 'unit-test-domain-cf18.com',
          'class' => 1,
          'ttl' => 60,
          'data' => [
            'type' => 1,
            'value' => '111.222.111.111']
        ]],
/*      'question domain TXT record'      => ['qname' => 'unit-test-domain-cf18.com', 'qtype' => 16, 'qclass' => 1],
      'question domain CNAME record'    => ['qname' => 'unit-test-domain-cf18.com', 'qtype' => 5, 'qclass' => 1],
      'question subdomain A record'     => ['qname' => 'subdomain.unit-test-domain-cf18.com', 'qtype' => 1, 'qclass' => 1],
      'question subdomain TXT record'   => ['qname' => 'subdomain.unit-test-domain-cf18.com', 'qtype' => 16, 'qclass' => 1],
      'question subdomain CNAME record' => ['qname' => 'subdomain.unit-test-domain-cf18.com', 'qtype' => 5, 'qclass' => 1]*/
    ]];
  }

  /**
   * @dataProvider DNSQuestionsProvider
   */
  public function testGetAnswer($hQuestion, $hExpectedResult) {
    $this->assertEquals($hExpectedResult, $this->storage->get_answer($hQuestion));
  }
}
