<?php

namespace yswery\DNS;

use PDO;

class MySQLStorageProvider extends AbstractStorageProvider {
  const HOSTNAME = 'database';
  const USER = 'root';
  const PASSWORD = 'cloudfest';
  const CHARSET = 'utf8mb4';
  const PORT = 3306;

  /**
   * @var PDO the database connection
   */
  private $PDO;

  /**
   * @var int
   */
  private $DS_TTL;

  /**
   * @var boolean
   */
  private $bRecursionAvailable;

  public function __construct($sDB, $iDefaultTTL = 300) {
    $sDSN = "mysql:host=" . self::HOSTNAME . ";port=" . self::PORT . ";dbname=$sDB;charset=" . self::CHARSET;
    $hOPT = [
      PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
      PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
      PDO::ATTR_EMULATE_PREPARES   => false,
      PDO::ATTR_PERSISTENT => true
    ];
    $this->PDO = new PDO($sDSN, self::USER, self::PASSWORD, $hOPT);

    $this->DS_TTL = $iDefaultTTL;
    $this->bRecursionAvailable = false;
  }

  /**
   * Only works with first-level subdomains and with TLDs with one . or in the form of co.*
   *
   * @param string $sHostName the full hostname to split into subdomain / domain
   *
   * @return array first element is the subdomain, second element is the domain
   */
  private function get_domain($sHostName) {
    $aParts = explode(".", $sHostName);
    if ( count($aParts) === 2 ) {
      return ["", $sHostName];
    }
    if ( $aParts[count($aParts) - 2] === 'co' ) {
      if ( count($aParts) === 3 ) {
        return ["", $aParts[count($aParts) - 3] . '.' . $aParts[count($aParts) - 2] . "." . $aParts[count($aParts) - 1]];
      } else {
        return [$aParts[count($aParts) - 4], $aParts[count($aParts) - 3] . '.' . $aParts[count($aParts) - 2] . "." . $aParts[count($aParts) - 1]];
      }
    }

    return [$aParts[count($aParts) - 3], $aParts[count($aParts) - 2] . "." . $aParts[count($aParts) - 1]];
  }

  /**
   * @param $aDNSQuestion
   *
   * @return array contains a hash of
   */
  public function get_answer($aDNSQuestion) {
    $sQHostName = $aDNSQuestion[0]['qname'];
    $sQRecordType = $aDNSQuestion[0]['qtype'];
    $sQClass = $aDNSQuestion[0]['qclass'];
    $sQHostName = trim($sQHostName, '.');
    list($sSubdomain, $sDomain) = $this->get_domain($sQHostName);

    $sRecordType = RecordTypeEnum::get_name($sQRecordType);
    // If there is no resource record or the record does not have the type, return an empty array.
    $oStatement = $this->PDO->prepare('SELECT target FROM nameservers JOIN domains ON (nameservers.domain_id=domains.id) WHERE domains.name=? AND nameservers.type=? AND nameservers.hostname=?');

    $oStatement->execute([$sDomain, $sRecordType, $sSubdomain]);
    if ( empty($oStatement->rowCount()) ) {
      return [];
    }
    $hAnswer = [];
    foreach( $oStatement as $hRow ) {
      $hAnswer[] = [
        'name' => $aDNSQuestion[0]['qname'],
        'class' => $sQClass,
        'ttl' => 60,
        'data' => [
          'type' => $sQRecordType,
          'value' => $hRow['target']
        ]
      ];
    }
    return $hAnswer;
  }

  /**
   * Getter method for $recursion_available property
   *
   * @return boolean
   */
  public function allows_recursion() {
    return $this->bRecursionAvailable;
  }

  /**
  * Check if the resolver knows about a domain
  *
  * @param  string  $sHostName the hostname to check for
   *
  * @return boolean            true if the resolver holds info about $domain, false otherwise
  */
  public function is_authority($sHostName) {
    $sHostName = trim($sHostName, '.');
    list(, $sHostName) = $this->get_domain($sHostName);
    $oStatement = $this->PDO->prepare('SELECT * FROM domains WHERE name = ?');
    $oStatement->execute([$sHostName]);
    return ($oStatement->rowCount() ? TRUE : FALSE);
  }
}
