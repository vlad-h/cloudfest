<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Nameserver extends Model
{
  protected $table = 'nameservers';

  protected $fillable = array('domain_id','hostname','type', 'target');

  public function nameservers()
  {
    return $this::hasMany(Domain::class);
  }

  public static function getPossibleDNSTypes()
  {
    $type = DB::select( DB::raw("SHOW COLUMNS FROM nameservers WHERE Field = 'type'") )[0]->Type;
    preg_match('/^enum\((.*)\)$/', $type, $matches);
    $enum = array();
    foreach( explode(',', $matches[1]) as $value )
    {
      $v = trim( $value, "'" );
      $enum = array_add($enum, $v, $v);
    }
    return $enum;
  }
}
