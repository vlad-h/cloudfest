<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/domains', function () {
    $domains = \App\Domain::where('user_id', '=', Auth::user()->id)
      ->get();
    return view('domains', ['domains' => $domains]);
})->name('domains')->middleware('auth');

use Illuminate\Http\Request;

Route::post('/add_domain', function (Request $request) {
    $data = $request->validate([
      'user_id' => 'integer',
      'name' => 'required|unique:domains|max:255',
    ]);

    $domain = new \App\Domain;
    $domain->name = $data['name'];
    $domain->user_id = Auth::user()->id;
    $domain->save();
    $domain_id = $domain->id;

    if ( !is_null($request->create_txt) && $request->create_txt == '1' ) {
        $nameserver = new \App\Nameserver;
        $nameserver->domain_id = $domain_id;
        $nameserver->hostname = '';
        $nameserver->type = 'TXT';
        $nameserver->target = 'domainconnect.cloudfest2018.local';
        $nameserver->save();
    }

    return redirect('/domains');
});

Route::get('/nameservers/{nameserver}', function($ns_id) {
    $domain = App\Domain::where('id', '=', $ns_id)->get();
    $types = App\Nameserver::getPossibleDNSTypes();
    $namerservers = \App\Nameserver::where('domain_id', '=', $ns_id)->get();
    return view('nameservers', ['nameservers' => $namerservers, 'domain' => $domain[0], 'types' => $types]);
})->name('nameservers')->middleware('auth');

Route::post('/add_nameserver/{domain_id}', function (Request $request) {
    $data = $request->validate([
      'hostname' => 'string|max:255',
      'type' => 'in:A,NS,CNAME,SOA,PTR,MX,AAAA,OPT,AXFR,ANY,AFSDB,APL,CAA,CDNSKEY,CDS,CERT,DHCID,DLV,DNSKEY,DS,IPSECKEY,
                                KEY,KX,LOC,NAPTR,NSEC,NSEC3,NSEC3PARAM,RRSIG,RP,SIG,SRV,SSHFP,TA,TKEY,TLSA,TSIG,TXT,URI,DNAME|required|max:200',
      'target' => 'required|max:255',
    ]);

    $nameserver = new \App\Nameserver;
    $nameserver->domain_id = $request->domain_id;
    $nameserver->hostname = $data['hostname'];
    $nameserver->type = $request->type;
    $nameserver->target = $data['target'];
    $nameserver->save();

    return redirect('/nameservers/'.$request->domain_id);
});