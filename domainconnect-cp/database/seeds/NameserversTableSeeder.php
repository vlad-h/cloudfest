<?php

use Illuminate\Database\Seeder;

class NameserversTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Nameserver::class, 5)->create()->each(function ($u) {
            $domain = new \App\Nameserver();
            $domain->domain_id = $u->domain_id;
            $domain->hostname = '_domainconnect';
            $domain->type = 'TXT';
            $domain->target = 'domainconnect.cloudfest2018.local';
            $domain->save();
        });
    }
}
