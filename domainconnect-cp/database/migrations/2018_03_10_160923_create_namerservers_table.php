<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNamerserversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nameservers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('domain_id')->index()->unsigned();
            $table->foreign('domain_id')->references('id')->on('domains');
            $table->string('hostname');
            $table->enum('type', ['A', 'NS', 'CNAME', 'SOA', 'PTR', 'MX', 'AAAA', 'OPT', 'AXFR', 'ANY', 'AFSDB', 'APL', 'CAA', 'CDNSKEY',
                                'CDS', 'CERT', 'DHCID', 'DLV', 'DNSKEY', 'DS', 'IPSECKEY', 'KEY', 'KX', 'LOC', 'NAPTR', 'NSEC', 'NSEC3', 'NSEC3PARAM',
                                'RRSIG', 'RP', 'SIG', 'SRV', 'SSHFP', 'TA', 'TKEY', 'TLSA', 'TSIG', 'TXT', 'URI', 'DNAME']);
            $table->string('target');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nameservers');
    }
}
