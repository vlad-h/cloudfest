<?php

use Faker\Generator as Faker;

$factory->define(App\Domain::class, function (Faker $faker) use ($factory) {
    return [
      'user_id' => $factory->create(App\User::class)->id,
      'name' => $faker->domainName,
    ];
});
