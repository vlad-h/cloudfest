<?php

use Faker\Generator as Faker;

$factory->define(App\Nameserver::class, function (Faker $faker) use ($factory) {
    return [
      'domain_id' => $factory->create(App\Domain::class)->id,
      'hostname' => '',
      'type' =>  $faker->randomElement(['A', 'CNAME']),
      'target' => $faker->randomElement(['127.0.0.1', '111.111.0.1', '112.112.0.1', '111.112.0.1', '111.112.0.2']),
    ];
});
