@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="domains">
                    @foreach ($nameservers as $nameserver)
                        <div class="col-md-4 col-form-label float-sm-left">{{ empty($nameserver->hostname) ? '-' : $nameserver->hostname }}</div>
                        <div class="col-md-4 col-form-label float-sm-left">{{ $nameserver->type }}</div>
                        <div class="col-md-4 col-form-label float-sm-left">{{ $nameserver->target }}</div>
                        <div style="clear: both"></div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-12">
                <h1>Add a new nameserver for {{$domain->name}}</h1>
                <form action="/add_nameserver/{{$domain->id}}" method="post">
                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            Please fix the following errors
                        </div>
                    @endif
                       
                    {!! csrf_field() !!}
                    <div class="form-group{{ $errors->has('hostname') ? ' has-error' : '' }}">
                        <label for="hostname">Hostname</label>
                        <input type="text" class="form-control" id="hostname" name="hostname" placeholder="Hostname" value="{{ old('hostname') }}">
                        @if($errors->has('hostname'))
                            <span class="help-block">{{ $errors->first('hostname') }}</span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                        <label for="type">Type</label>
                        <select class="form-control m-bot15" name="type">
                            @if(count($types) > 0)
                                @foreach($types as $type)
                                    <option value="{{$type}}">{{$type}}</option>
                                @endForeach
                            @else
                                No Record Found
                            @endif
                        </select>
                    </div>
                    <div class="form-group{{ $errors->has('target') ? ' has-error' : '' }}">
                        <label for="target">Target</label>
                        <input type="text" class="form-control" id="target" name="target" placeholder="Target" value="{{ old('target') }}">
                        @if($errors->has('taget'))
                            <span class="help-block">{{ $errors->first('target') }}</span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-default">Add</button>
                </form>
            </div>
            <div class="row">
                <a href="/domains">Back to domains list</a>
            </div>
        </div>
    </div>
@endsection