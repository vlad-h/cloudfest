@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @auth
                        <a href="{{ route('domains') }}">Domains</a>
                    @endauth
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
