@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="domains">
                    @foreach ($domains as $domain)
                        <div class="col-md-4 col-form-label float-sm-left">{{ $domain->name }}</div>
                        <div class="col-md-4 col-form-label float-sm-right"><a href="nameservers/{{$domain->id}}">See namerservers</a></div>
                        <div style="clear: both"></div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-12">
                <h1>Add a new domain</h1>
                <form action="/add_domain" method="post">
                    @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            Please fix the following errors
                        </div>
                    @endif

                    {!! csrf_field() !!}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Domain</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Domain" value="{{ old('name') }}">
                        @if($errors->has('name'))
                            <span class="help-block">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="create_txt">Auto create TXT for domainconnect</label>
                        <input checked="checked" name="create_txt" type="checkbox" value="1">
                    </div>
                    <button type="submit" class="btn btn-default">Add</button>
                </form>
            </div>
        </div>
    </div>
@endsection