Domain Connect With Local DNS Server
==============

A dockerized version of Domain Connect with Service Provider, DNS Provider and local DNS Server.

This repository contains the Dockerfiles and the source files for Service Provider, DNS Provider and DNS Server.

# Directory layout

* `docker-images` contain the Dockerfile images for every service
* `dnsserver` a opensource DNS server in PHP https://github.com/yswery/PHP-DNS-SERVER repository
* `domainconnect` the code for Domain Connect API where the Service Provider is asking for the DNS configuration
* `domainconnect-ux` the contents of this folder is what the the user sees when he need to confirm applying template
* `serviceprovider` example implementation of a Service Provider from https://github.com/Domain-Connect/python-dc-statelesshosting

# Quickstart

1 Clone this repository

  ```shell
  https://vlad-h@bitbucket.org/vlad-h/cloudfest.git
  ```
2 Run the docker compose

  ```shell
  docker-compose up -d
  ```
  
3 Add this line in you *host* file

  ```shell
  127.0.0.1 domainconnect.cloudfest.local
  127.0.0.1 domainconnect-ux.cloudfest.local
  127.0.0.1 serviceprovider.cloudfest.local
  127.0.0.1 dbinterface.cloudfest.local
  ```
