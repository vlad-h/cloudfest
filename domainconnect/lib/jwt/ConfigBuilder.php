<?php

class ConfigBuilder{

    protected $config = NULL;
    protected $docker_config = NULL;

    public function __construct()
    {

        if(getenv('DOCKER_DEVELOP_BOX') !== FALSE) {
            //DOCKER CONFIG SETUP
            $this->docker_config['issuer'] = 'testIssuer';
            $this->docker_config['jwt_algorithm'] = 'HS256';
            $this->docker_config['jwt_key'] = '#fdaf$R$4&4#%g)*3WFG!';
        } else {
            //@TODO: LIVE CONFIG SETUP
            $this->config['issuer'] = 'kis.hosteurope.de';
            $this->config['jwt_algorithm'] = 'HS256';
            $this->config['jwt_key'] = 'LIHDNlerjn`v&$Fvfkby986+';
        }
    }

    public function getConfig(){
        if(getenv('DOCKER_DEVELOP_BOX') !== FALSE) {
            $config =  $this->docker_config;
        } else {
            $config =  $this->config;
        }

        //Check if all array values are set
        try {
            if (!in_array(NULL,$config) && isset($config)) {
                return $config;
            }else{
                throw new Exception("Please set the configuration keys properly!");
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }
}