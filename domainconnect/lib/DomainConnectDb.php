<?php

class DomainConnectDb
{
    /**
     * @var object
     */
    private $_db;

    /**
     * @var object
     */
    private static $_instance;

    /**
     * Get an instance of the DomainConnectDb Class
     *
     * @return Instance
     */
    public static function getInstance() {
        if(!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Class constructor that is declared as protected to prevent creating a new instance outside of the class via the new operator.
     */
    protected function __construct() {
        $this->db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASS);
    }

    /**
     * Magic method clone is empty to prevent duplication of connection
     */
    private function __clone() { }


    /**
     * Query the database to check if a domain with tld belongs to a customer
     *
     * @param integer $kdnummer
     * @param string $domainname
     * @param string $tld
     * @return bool
     */
    public function domainBelongsToCustomer($user_id, $domain) {
        $sth = $this->db->prepare("SELECT * FROM domains WHERE name = :domain AND user_id = :user_id");
        $sth->execute(array(':domain' => $domain, ':user_id' => $user_id));
        return (bool)$sth->rowCount();
    }

    /**
     * Check if the domain exists
     * @param $domain
     * @return bool
     */
    public function domainExists($domain){
        $sth = $this->db->prepare("SELECT * FROM domains WHERE name = :domain");
        $sth->execute(array(':domain' => $domain));
        return (bool)$sth->rowCount();
    }


    /**
     * Insert the DNS records from the template in the database
     *
     * @param array $params
     * @param array $templateRecords
     * @return bool
     */
    public function addDNSRecords($params, $templateRecords){
        $domainId = $this->getDomainId($params->domain);

        foreach($templateRecords as $recordData){
            //We only supprt the records that are given in $this->nsRecords (A,AAAA,CNAME,TXT)

            $pointer = '';
            $host = '';
            $ttl = $recordData->ttl ? (int)$recordData->ttl : null;
            if($recordData->pointsTo){
                $pointer = $recordData->pointsTo;
            } else if($recordData->data){
                $pointer = $recordData->data;
            }
            // If pointer value equal to @ change with the domain
            if($pointer == '@'){
                $pointer = $params->domain;
            }

            if(isset($recordData->host) && $recordData->host != '@'){
                $host = $recordData->host;
            } else if (isset($params->host) && !empty($params->host)){
                $host = $params->host;
            }

            $sth = $this->db->prepare("INSERT INTO nameservers(domain_id, hostname, type, target) VALUES(:domain_id, :hostname, :type, :target)");
            $sth->execute(array(':domain_id' => $domainId, ':hostname' => $host, ':type' => $recordData->type, ':target' => $pointer));
            if(empty($host)){
                $sth = $this->db->prepare("INSERT INTO nameservers(domain_id, hostname, type, target) VALUES(:domain_id, :hostname, :type, :target)");
                $sth->execute(array(':domain_id' => $domainId, ':hostname' => 'www', ':type' => $recordData->type, ':target' => $pointer));
            }
        }

        return TRUE;
    }

  /**
   * Return the ID of a domain, based on its name
   *
   * @param string $domain name of the domain
   * @return int|mixed -1 if the domain is not found, otherwise its id from the database
   */
    private function getDomainId($domain) {
      $sth = $this->db->prepare("SELECT id FROM domains WHERE name = :domain");
      $sth->execute(array(':domain' => $domain));

      return $sth->fetchColumn();

    }

    /**
     * Get a list of the current DNS records that will be deleted after the template records are inserted
     *
     * @param array $params
     * @param array $templateRecords
     * @return array
     */
  public function getCurrentDNSRecordsForClearing($params, $templateRecords) {

    $listIds = [];
    $domainId = $this->getDomainId($params->domain);

    foreach ( $templateRecords as $recordData ) {

      $type = $recordData->type;
      if ( in_array($recordData->type, array('A', 'AAAA')) ) {
        $type = 'A';
        $type2 = 'AAAA';
        $type3 = 'ACNAME';
      }

      $sql = "SELECT * FROM nameservers WHERE domain_id = :domain AND (type = :type";
      $sql .= isset($type2) ? " OR type = :type2 OR type = :type3)" : ")";

      $sth = $this->db->prepare($sql);
      $sth->bindParam(':domain', $domainId, PDO::PARAM_INT);
      $sth->bindParam(':type', $type, PDO::PARAM_STR);
      if ( isset($type2) ) {
        $sth->bindParam(':type2', $type2, PDO::PARAM_STR);
        $sth->bindParam(':type3', $type3, PDO::PARAM_STR);
      }
      $sth->execute();
      if ( !$sth->rowCount() ) {
        continue;
      }
      $host = '';
      if ( isset($recordData->host) && $recordData->host != '@' ) {
        $host = $recordData->host;
      } else {
        if ( isset($params->host) && !empty($params->host) ) {
          $host = $params->host;
        }
      }
      $sql = "SELECT id FROM nameservers WHERE domain_id = :domain AND (type = :type";
      $sql .= isset($type2) ? " OR type = :type2 OR type = :type3)" : ")";
      $sql .= " AND (hostname=:hostname";
      $sql .= empty($host) ? " OR hostname = :www)" : ")";
      $sth = $this->db->prepare($sql);
      $sth->bindParam(':domain', $domainId, PDO::PARAM_INT);
      $sth->bindParam(':hostname', $host, PDO::PARAM_STR);
      $sth->bindParam(':type', $type, PDO::PARAM_STR);
      $sth->bindParam(':type2', $type2, PDO::PARAM_STR);
      $sth->bindParam(':type3', $type3, PDO::PARAM_STR);
      if ( empty($host) ) {
        $www = 'www';
        $sth->bindParam(':www', $www, PDO::PARAM_STR);
      }
      $sth->execute();
      if ( !$sth->rowCount() ) {
        continue;
      } else {
        foreach ( $sth as $row ) {
          $listIds[] = $row['id'];
        }
      }
    }

    return $listIds;
  }

    /**
     * Clear/Delete DNS records by ID and other conditions
     *
     * @param array $clearIds
     * @param array $params
     */
    public function clearDNSRecords($clearIds){
        if(empty($clearIds)){
            return;
        }

      $list_of_ids = implode(',', $clearIds);
      $sth = $this->db->query("DELETE FROM nameservers WHERE id IN (".$list_of_ids.")");
      $sth->execute();
    }
}