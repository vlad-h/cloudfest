<?php

/**
 * @method static Router get(string $route, Callable $callback)
 * @method static Router post(string $route, Callable $callback)
 * @method static Router put(string $route, Callable $callback)
 * @method static Router delete(string $route, Callable $callback)
 * @method static Router options(string $route, Callable $callback)
 * @method static Router head(string $route, Callable $callback)
 */
class Router
{

    private static $routes = array();
    private static $methods = array();
    private static $callbacks = array();

    public static function __callstatic($method, $params){
        $uri = $params[0];
        $callback = $params[1];

        array_push(self::$routes, $uri);
        array_push(self::$methods, strtoupper($method));
        array_push(self::$callbacks, $callback);
    }

    /**
     * Runs the callback for the given request
     */
    public static function dispatch(){
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $method = $_SERVER['REQUEST_METHOD'];

        $found_route = false;

        foreach(self::$routes as $key=>$route){
            if (strpos($route, ':') !== false) {
                $route = str_replace(':param', '.*', $route);
            }

            if (preg_match('#^' . $route . '#i', rtrim($uri, '/ '), $matched)) {

                array_shift($matched);
                if (count($matched)>0 && strpos($matched[count($matched) - 1], '/') !== false) {
                    continue;
                }
                if (self::$methods[$key] == $method || self::$methods[$key] == 'ANY') {
                    $found_route = true;

                    if (is_object(self::$callbacks[$key])) {
                        $query = array();
                        parse_str($_SERVER['QUERY_STRING'], $query);
                        $matched[] = $query;

                        call_user_func_array(self::$callbacks[$key], $matched);

                        return;
                    }
                }
            }
        }
        
        if ($found_route == false) {
            header($_SERVER['SERVER_PROTOCOL']." 404 Not Found", true, 404);
            echo '404 Not Found';
            die;
        }
    }
}