<?php

require_once( 'DomainConnectDb.php' );

class DomainConnectApi
{
    /**
     * @var NULL
     */
    private $templatesDirectory = NULL;
    /**
     * @var string
     */
    private $urlSyncUX = "http://domainconnect-ux.cloudfest.local";
    /**
     * @var string
     */
    private $urlAPI = "http://domainconnect.cloudfest.local";

    /**
     * Class constructor
     */
    public function __construct() {
      $this->templatesDirectory = __DIR__."/Templates/";
    }

    /**
     * Can then be queried to gather the necessary information for about the service
     * @url {API_URL}/v2/hosteurope.de/settings
     *
     * @param string $domain
     * @return string
     */
    public function getDomainSettings($domain){
        if(!DomainConnectDb::getInstance()->domainExists($domain)){
            http_response_code(404);
            die('INVALID DOMAIN');
        }
        $stub = json_encode(array(
                "providerName"=>"cloudfest",
                "domain"=>$domain,
                "urlSyncUX"=>$this->urlSyncUX,
                "urlAPI"=>$this->urlAPI,
                "redirectSupported" =>true,
                "width"=> 726,
                "height"=> 433
            )
        );

        return $stub;
    }

    /**
     * Check if requested template is valid
     * @url  {API_URL}/v2/domainTemplates/providers/{providerId}/services/{serviceId}
     *
     * @param string $providerId
     * @param string $serviceId
     * @return void
     */
    public function getDomainTemplates($providerId, $serviceId){
        if($this->templateExists($providerId,$serviceId)){
            //header(':', true, 200);
            http_response_code(200);
        }else{
            //header(':', true, 404);
            http_response_code(404);
        }
    }

    /**
     * Update DNS records for specific template
     * @url {API_URL}/v2/domainTemplates/providers/{providerId}/services/{serviceId}/update
     *
     * @throws Exception if the template not exists
     * @throws Exception if the jwt_token data not exists
     *
     * @param string $providerId
     * @param string $serviceId
     * @param array $applyData
     * @return void
     */
    public function updateDomainTemplates($providerId, $serviceId, $applyData = array()){
      require_once('jwt/DC_JWT.php');
      try {
          $templateContent = $this->templateExists($providerId, $serviceId);
          if (!$templateContent) throw new Exception("Please set the template correctly!");

          $jwt = new DC_JWT(new ConfigBuilder());
          if (isset($applyData['jwt_token'])) {
              $decodedData = $jwt->decode($applyData['jwt_token']);
              $verif = $this->makeDnsChanges($decodedData->data, $templateContent);
              if($verif instanceof Exception){
                  throw $verif;
              }
              return TRUE;
          }else{
              throw new Exception("Please set applyData[jwt_token] properly!");
          }
      } catch (\Exception $e) {
            throw $e;
      }
    }

    /**
     * Validate the update params and call the methods to add the DNS records and clear
     * the old ones if everything it's inserted correctly
     *
     * @throws Exception if the params are not valid
     * @throws Exception an error occured when the DNS records are inserted
     *
     * @param array $updateParams
     * @param string $templateContent
     * @return true|Exception
     */
    private function makeDnsChanges($updateParams, $templateContent) {
        if(!$this->validateUpdateParams($updateParams, $templateContent)){
            return new Exception("Please validate UpdateParams properly!");
        }
        // Removing the domain from subdomain field and leave only subdomain if exists
        if(isset($updateParams->host)){
            $updateParams->host = str_replace('.'.$updateParams->domain, '', $updateParams->host);
        }

        $this->replaceTemplateParams($updateParams, $templateContent->records);

        $clearIds = DomainConnectDb::getInstance()->getCurrentDNSRecordsForClearing($updateParams, $templateContent->records);

        $res = DomainConnectDb::getInstance()->addDNSRecords($updateParams, $templateContent->records);
        if(!$res){
            return new Exception("Please recheck the add of DNS Records properly!");
        }
        if(!empty($clearIds)) {
            DomainConnectDb::getInstance()->clearDNSRecords($clearIds, $updateParams);
        }

        return TRUE;
    }


    /**
     * Check if template exists and return the decoded template contents
     *
     * @param string $providerId
     * @param string $serviceId
     * @return bool|array
     */
    private function templateExists($providerId, $serviceId) {
        $templatePath = $this->templatesDirectory . $providerId . "." . $serviceId . ".json";
        if (@file_exists($templatePath)) {
            $templateContent = @json_decode(@file_get_contents($templatePath));
            if (!$templateContent){
                return FALSE;

            }
            return $templateContent;
        }else{
            return FALSE;
        }
    }


    /**
     * Validate that the value that needs to be replaced from the $templateContent exists in $params
     * an call the method that validate if the domains belongs to a customer
     *
     * @param array $params
     * @param mixed $templateContent
     * @return bool
     */
    private function validateUpdateParams($params, $templateContent){
        if(!isset($params->domain) && !isset($params->user_id)){
            return FALSE;
        }

        if(!DomainConnectDb::getInstance()->domainBelongsToCustomer($params->user_id, $params->domain)){
            return FALSE;
        }

        foreach($templateContent->records as &$record){
            foreach($record as &$value){
                if(preg_match('/%(.*)%/', $value, $matches)){
                    if(!isset($params->{$matches[1]})){
                        return FALSE;
                    }
                }
            }
        }
        return TRUE;
    }

    /**
     * Replace template records with parameters values
     *
     * @param array $params
     * @param array &$records modified records
     * @return void
     */
    private function replaceTemplateParams($params, &$records){
        foreach($records as &$record){
            foreach($record as &$value){
                if(preg_match('/%(.*)%/', $value, $matches)){
                    if(isset($params->{$matches[1]})){
                        $value = str_replace($matches[0], $params->{$matches[1]}, $value);
                    }
                }
            }
        }
    }

}