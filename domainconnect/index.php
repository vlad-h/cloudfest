<?php
header('Access-Control-Allow-Origin: *');

require_once( 'config.php');
require_once( 'lib/Router.php');
require_once( 'lib/DomainConnectApi.php');

/*
 * Necessary information about the service
 * /v2/{domain}/settings
 */

Router::get('/v2/(:param)/settings', function($domain){
    // **** DomainConnectApi call ****
    try{
        $dcObj = new DomainConnectApi();
        header('Content-type: application/json');
        echo $dcObj->getDomainSettings($domain);
    } catch (Exception $e) {
        die($e->getMessage());
    }
});


/*
 * Verify if a template exists on DomainConnect
 * /v2/domainTemplates/providers/{providerId}/services/{serviceId}/
 *
 */
Router::get('/v2/domainTemplates/providers/(:param)/services/(:param)', function($providerId, $servideId){
    //echo 'GET request for /v2/domainTemplates/providers/{providerId}/services/{serviceId}';
    try{
        $dcObj = new DomainConnectApi();
        $dcObj->getDomainTemplates($providerId, $servideId);
    } catch (Exception $e) {
        die($e->getMessage());
    }
});

Router::get('', function(){
    header('HTTP/1.1 200');
});
/*
 * Once the user has confirmed the application of the DNS record changes, an AJAX request to `/update` will handle to details of the configuration.
 * /v2/domainTemplates/providers/{providerId}/services/{serviceId}/update
 *
 */
Router::post('/v2/domainTemplates/providers/(:param)/services/(:param)/update', function( $providerId, $serviceId){
    try{
        $dcObj = new DomainConnectApi();
        header('Content-Type: application/json');
        $applyData = $_POST;
        $response = $dcObj->updateDomainTemplates($providerId, $serviceId,$applyData);
        if($response === FALSE) {
          header('HTTP/1.1 400 Bad Request');
          echo json_encode(array('status' => 'error'));
          return false;
        }
        echo json_encode(array('status' => 'success'));
    } catch (Exception $e) {
        header('HTTP/1.1 400 Bad Request');
        echo json_encode(array('status' => 'error', 'message' => $e->getMessage()));
        return false;
    }

});


Router::dispatch();