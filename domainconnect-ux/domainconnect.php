<?php
define('DB_HOST', 'database');
define('DB_NAME', 'domainconnect');
define('DB_USER', 'root');
define('DB_PASS', 'cloudfest');
define('URL_API', 'http://domainconnect.cloudfest.local');

require_once('lib/openssl/OpenSslSignature.php');
require_once('lib/jwt/DC_JWT.php');
require_once('lib/LoginHandler.php');


if ( ! isset($_GET['domain']) && empty($_GET['domain'])) {
    http_response_code(400);
    die('400 Bad Request');
}

session_start();

$uri = strstr($_SERVER['REQUEST_URI'],"apply?",1);
$viewData = [
    'api_call' => URL_API . $uri .'update'
];

if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') { 
    $object = LoginHandler::getInstance();
    $error = '';

    if (isset($_POST['username']) && isset($_POST['password']) && isset($_GET['domain'])) {
        $_SESSION['loggedin'] = $object->checkLoginWithDomain($_POST['username'], $_POST['password'], $_GET['domain'], $error);

        if (empty($_SESSION['loggedin'])) {
            $viewData['error'] = $error;
        }
    }
}

if ( ! empty($_SESSION['loggedin'])) {
    $object = LoginHandler::getInstance();
    $domainBelongToUser = $object->domainBelongsToUser($_GET['domain'], $_SESSION['loggedin']);

    if (empty($domainBelongToUser)) {
        require_once ('view/login.php');
        session_destroy();
        exit();
    } else {   
        $jwt = new DC_JWT(new ConfigBuilder());
        $jwt_data = $jwt->encode(array_merge($_GET, array('user_id' => $_SESSION['loggedin'])));

        $viewData['params'] = ['jwt_token' => $jwt_data];
        $viewData['domain'] = $_GET['domain'];

        require_once ('view/confirm.php');
    }
} else {
    require_once ('view/login.php');
}