<?php
use \Firebase\JWT\JWT;

require_once('vendor/autoload.php');
require_once('ConfigBuilder.php');
class DC_JWT {

  protected $encryptKey = NULL;
  protected $encryptAlgorithm = NULL;
  protected $token = NULL;
  protected $config = NULL;

  public function __construct(ConfigBuilder $configBuilder) {
    $this->config = $configBuilder->getConfig();
    $this->encryptKey = $this->config['jwt_key'];
    $this->encryptAlgorithm = $this->config['jwt_algorithm'];
    $this->token = array();
    $this->token['iss'] = $this->config['issuer'];
    $this->token['iat'] = time();
    $this->token['exp'] = strtotime('+10 minutes');
  }

  public function encode($data) {
    $this->token['data'] = $data;
    return JWT::encode($this->token,$this->encryptKey,$this->encryptAlgorithm);
  }

  public function decode($jwtToken) {
    return JWT::decode($jwtToken,$this->encryptKey,array($this->encryptAlgorithm));
  }

}