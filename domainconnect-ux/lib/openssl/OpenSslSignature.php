<?php

class OpenSslSignature {

  protected $domain;
  protected $rawPublicKey;
  protected $publicKey;
  protected $signature;
  protected $data;
  protected $algorithm;
  protected $supportedAlgorithms;

  public function __construct($domain,$data,$signature,$publicKey = NULL) {
    $this->supportedAlgorithms = array(
      'RS512' => 'SHA512',
      'RS384' => 'SHA386',
      'RS256' => 'SHA256',
      'RS224' => 'SHA224'
    );
    $this->domain = $domain;
    $this->data = $data;
    $this->signature = strpos($signature, "\0") === FALSE ? base64_decode($signature) : $signature;
    $this->rawPublicKey = $publicKey;
    $this->algorithm = $this->supportedAlgorithms['RS256'];
    $this->publicKey = !is_null($this->rawPublicKey) ? $this->loadKey() : $this->loadPublicKeyFromTxtRecord();
  }

  private function loadPublicKeyFromTxtRecord() {
      if(isset($_GET['key'])) {
      $dns_records = @dns_get_record($_GET['key'].'.'.$this->domain, DNS_TXT);
      if(is_array($dns_records)) {
        $parts = array();
        $selAlgorithm = 'RS256';
        foreach($dns_records as $dns_record) {
          if(preg_match('/^p=(?<part>\d+),a=(?<algorithm>.*?),d=(?<data>.+)/', $dns_record['txt'], $matches)){
            $parts[$matches['part']-1] = $matches['data'];
            $selAlgorithm = $matches['algorithm'];
          }
        }
        ksort($parts);
        $this->rawPublicKey = join('', array_values($parts));
        $this->matchAlgorithm(strtoupper($selAlgorithm));
        return $this->loadKey();
      }
    }
  }

  private function loadKey() {
    $key = $this->rawPublicKey;
    $key = str_replace('-----BEGIN PUBLIC KEY-----','',$key);
    $key = str_replace('-----END PUBLIC KEY-----','',$key);
    $key = trim($key);
    $key = "-----BEGIN PUBLIC KEY-----\n".
           wordwrap($key, 64, "\n", TRUE).
           "\n-----END PUBLIC KEY-----";
    return @openssl_pkey_get_public($key);
  }

  private function matchAlgorithm($algoKey = NULL) {
    if($algoKey != NULL) {
      if(isset($this->supportedAlgorithms[$algoKey])) $this->algorithm = $this->supportedAlgorithms[$algoKey];
    }
  }

  public function check() {
    return @openssl_verify($this->data,$this->signature,$this->publicKey,$this->algorithm) == 1 ? TRUE : FALSE;
  }

}

//$publicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA18SgvpmeasN4BHkkv0SBjAzIc4grYLjiAXRtNiBUiGUDMeTzQrKTsWvy9NuxU1dIHCZy9o1CrKNg5EzLIZLNyMfI6qiXnM+HMd4byp97zs/3D39Q8iR5poubQcRaGozWx8yQpG0OcVdmEVcTfyR/XSEWC5u16EBNvRnNAOAvZYUdWqVyQvXsjnxQot8KcK0QP8iHpoL/1dbdRy2opRPQ2FdZpovUgknybq/6FkeDtW7uCQ6Mvu4QxcUa3+WP9nYHKtgWip/eFxpeb+qLvcLHf1h0JXtxLVdyy6OLk3f2JRYUX2ZZVDvG3biTpeJz6iRzjGg6MfGxXZHjI8weDjXrJwIDAQAB';
//
//$sigCkC = new OpenSslSignature('testdespina.com', 'domain=testdespina.com&RANDOMTEXT=shm%3A1513253398%3Ayes&IP=132.148.25.185&host=testy','RmMy1RzkZZ7wKn6WsH47mnsViZidowyk6tLa4DWHkkxlkoFLFbTfSVmLQ7jhCpi6ZN4IFcfVyIjCLwxQ3RGs0oMrzjqLSU6Sj0LHiJ+N8uZdavJ2hU9vF4racE8XMYHKVQ8RaSig0qmj5aVvDChZmKZePA1sdOVSad8cJWBlQVNCH6L1pJwe4kTWY9R30569fuxo+wKTp/vB/78KC3SA3E+ktHlfsrT/DVm203oZBrQeDII/Gh01Zqzvm7o30/gNVH4THcQZFfMJ1wdtkaF2Q5c0QyZyeh3FB5CxhrKPm+B4qUzgqDEWL37GaTGdFstMbEw762dw/fyfoFItXirW6w==', $publicKey);
//echo ($sigCkC->check() ==1 )? 'great :)':'bad :(';
