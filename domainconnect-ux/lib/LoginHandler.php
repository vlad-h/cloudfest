<?php 

if ( ! class_exists('LoginHandler')) {
    class LoginHandler {
        /**
         * @var LoginHandler
         */
        public static $instance;

        /**
         * @var PDO Connection
         */
        protected $db;

        /**
         * Overiding php clone -- singleton purpose
         * @access private
         */
        private function __clone() {}

        /**
         * Singleton purpose (do not allow unserialize)
         */
        public function __wakeup()
        {
            throw new Exception("Cannot unserialize singleton!");
        }

        /**
         * Class constructor
         */
        private function __construct()
        {
            $this->db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8', DB_USER, DB_PASS);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        /**
         * @name getInstance
         * @description Gets the current class instance
         * 
         * @return LoginHandler $object
         */
        public static function getInstance() 
        {
            if (self::$instance == null) {
                self::$instance = new LoginHandler();
            }

            return self::$instance;
        }

        /**
         * @name checkLogin
         * @description Checks if the users login credentials are  okay
         *
         * @param string $username
         * @param string $password
         * @return mixed
         */
        public function checkLogin($username = '', $password = '')
        {
            $query = 'SELECT id FROM users WHERE email = :username AND password = :password LIMIT 1';
            $sth = $this->db->prepare($query);
            $sth->execute(array(':username' => $username, ':password' => md5($password)));
            $results = $sth->fetch(PDO::FETCH_ASSOC);

            if (count($results) > 0 && isset($results['id'])) {
                return $results['id'];
            }

            return false;
        }

        /**
         * @name domainBelongsToUser
         * @description Checks if the domain belong to the user
         *
         * @param string $domain
         * @param integer $userId
         * @return boolean
         */
        public function domainBelongsToUser($domain = '', $userId = 0)
        {
            $query = 'SELECT id FROM domains WHERE name = :domain AND user_id = :userId LIMIT 1';
            $sth = $this->db->prepare($query);
            $sth->execute(array(':domain' => $domain, ':userId' => $userId));
            $results = $sth->fetch(PDO::FETCH_ASSOC);

            if (count($results) > 0 && isset($results['id'])) {
                return true;
            }

            return false;
        }

        /**
         * @name checkLoginWithDomain
         * @description Checks the user's credentials are okay and if the domain belong to the specific user
         *
         * @param string $username
         * @param string $password
         * @param string $domain
         * @return mixed
         */
        public function checkLoginWithDomain($username = '', $password = '', $domain = '', &$error = '')
        {
            $userId = $this->checkLogin($username, $password);

            if ( ! empty($userId)) {
                $domainOk = $this->domainBelongsToUser($domain, $userId);

                if ( ! empty($domainOk)) {
                    return $userId;
                } else {
                    $error .= 'Domain does not belong to the user';
                }
            } else {
                $error .= 'Invalid user/password combination';
            }

            return false;
        } 
    }
}