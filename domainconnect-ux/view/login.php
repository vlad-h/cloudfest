<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<style>
    body {
        background: #eee !important;
    }

    .wrapper {
        margin-top: 80px;
        margin-bottom: 80px;
    }

    .form-signin {
        max-width: 450px;
        padding: 15px 35px 45px;
        margin: 0 auto;
        background-color: #fff;
        border: 1px solid rgba(0,0,0,0.1);

    .form-signin-heading{
        margin-bottom: 30px;
    }

    .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;

    input[type="text"] {
        margin-bottom: -1px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }

    input[type="password"] {
        margin-bottom: 20px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

</style>
<div class="wrapper">
    <form class="form-signin" method="post">
        <h2 class="form-signin-heading">DNS Provider Login</h2>
        <?php if(isset($viewData['error'])) : ?>
            <div class="alert alert-danger"><?php echo $viewData['error'] ?></div>
        <?php endif; ?>
        <div class="form-group">
            <input type="text" class="form-control" name="username" placeholder="Username" required="" autofocus="" />
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Password" required=""/>
        </div>
        <div class="form-group">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
        </div>
    </form>
</div>