<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<style>
    body {
        background: #eee !important;
    }

    .wrapper {
        margin-top: 80px;
        margin-bottom: 80px;
    }

    .confirm-box {
        max-width: 450px;
        padding: 15px 35px 45px;
        margin: 0 auto;
        background-color: #fff;
        border: 1px solid rgba(0,0,0,0.1);
    }

    p {
        font-size: 18px;
    }

</style>
<div class="wrapper">
    <div class="confirm-box">
        <h2 class="form-signin-heading">DNS Provider confirm</h2>
        <br>
        <p>DNS PRovider would like to make DNS changes to your domain "<strong><?php echo $viewData['domain'] ?></strong>".</p>
        <p>Click Confirm to make the changes.</p>
        <div class="pull-right">
            <button type="button" class="btn btn-primary confirm">CONFIRM</button>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    var params = <?php echo json_encode($viewData['params']); ?>;
    var apiCallUrl = "<?php echo $viewData['api_call']; ?>";

    var closeButton = '<button type="button" class="btn btn-primary close">CLOSE</button>';

    $('body').on('click', '.close', function(){
        window.close();
    });

    $('.confirm').on('click', function(){
        $.ajax({
            type: "POST",
            url: apiCallUrl,
            data: params,
        }).done(function(){
            <?php if(isset($_GET['redirect_uri'])) : ?>
            window.location = '<?php echo $_GET['redirect_uri']?>';
            <?php else: ?>
            //TODO: Success message
            $('.confirm-box').html('<p>THE DNS records have been successfully applied.</p>'+closeButton);
            <?php endif; ?>
        }).fail(function(jqXHR, textStatus){
            //TODO: Error message
            $('.confirm-box').html('<p>Failure to apply DNS records.</p>'+closeButton);
        });
    });
</script>
